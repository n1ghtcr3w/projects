sitename: tk1337

-

theme: standard/dark

-

brand: <svg width="1.8em" height="1.8em" viewBox="0 0 16 16" class="bi bi-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  <circle cx="8" cy="8" r="8"/>
</svg>

-

imageAppleTouchIcon: /shared/tkl.png

-

imageFavicon: /shared/tkl.png

-

imageLogo: /shared/tkl.png

-

ogImage: *.png, *.jpg, /shared/image-landscape.png

-

urlSearchResults: /
<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'automad/automad',
  ),
  'versions' => 
  array (
    'automad/automad' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'automad/lightbox' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dcb86247fb4f90fcaac150019f7906c506f20d0b',
    ),
    'automad/meta-tags' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ecf01d8dc978e4ca4f186c78c765ab12883cb49a',
    ),
    'automad/package-installer' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '6cefe27a0b7f6fc3432d08e8eeed9e6f6aac0e8b',
    ),
    'dahmen/automad-terminal' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '996cf9ad2ab4b00e0e1199f2c19d823a3f372728',
    ),
  ),
);

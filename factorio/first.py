
#--code help from:
# https://www.programiz.com/python-programming/type-conversion-and-casting
# https://stackoverflow.com/questions/385572/typecasting-in-python
# https://www.w3schools.com/python/python_variables.asp

import os

prev_val = 0
num_iter = 0
counter = 0
curr_num = 0
prev_num = 0
fact_in_int = -1
fact_in_str = ''
fact_out = 0
mode = 'y'

def cast_variables( ):
    fact_in_int = int( fact_in_str )
    num_iter = int( fact_in_str )
    counter = int( fact_in_str )
    return 0

def calc_to_n():
    curr_num = fact_in_int
    counter = counter
    while counter > 0:
        print(".")
        fact_out = curr_num * prev_num
        counter += -1
        prev_num = curr_num


print( "----------typecasing example-------------" )

while mode == 'y':
    fact_in_str = input( ":: please enter number to calculate factorial to : " )
    print( "calculating factorial to " + str( fact_in_str ))
    cast_variables()
    counter = counter
    print("......variables cast")
    calc_to_n()
    mode = input( "----do again ? (y) to continue----" )

print( "---------------end of typecast factorial----------" )
           

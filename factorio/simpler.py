
#--code help from:
# https://www.programiz.com/python-programming/type-conversion-and-casting
# https://stackoverflow.com/questions/385572/typecasting-in-python
# https://www.w3schools.com/python/python_variables.asp

import os

prev_val = 0
num_iter = 0
counter = 0
curr_num = 0
prev_num = 0
fact_in_int = -1
fact_in_str = ''
fact_out = 0
mode = 'y'

def calc_to( temp_fact_in ):
    num_times = int( temp_fact_in )
    curr_num = int( temp_fact_in )
    prev_num = 0
    fact_out = 1
    while num_times > 0:
        print(".")
        fact_out *= int(num_times)
        num_times += -1
        print( fact_out )
    return fact_out

print( "----------typecasing example-------------" )

while True:
    fact_in_str = input( ":: please enter number to calculate factorial to : " )
    fact_in_int = int( fact_in_str )
    print( "calculating factorial to " + str( fact_in_int ))
    prog_fact_out = calc_to( fact_in_int )
    print( "the factorial is:  " + str( prog_fact_out ))
    user_in = raw_input( "----do again ? (y) to continue----" )
    if( user_in.strip() != 'y'):
        break

print( "---------------end of typecast factorial----------" )
           

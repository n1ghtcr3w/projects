
#include <stdio.h>
#include <iostream>
using namespace std; 

int main(void){

  unsigned int data_to_sort[] = {
    0xBAD10002,
    0xCAD20002,
    0xBAC30002,
    0x00000002,
    0xBAD10001,
    0xFFFF0002,
    0xBAD10003,
    0x00000002,
    0x00000001,
    0xBBD20005
  };
 
 
 unsigned int truncated_array[10] = { 0,0,0,0,0,0,0,0,0,0 } ;

 //truncate data_to_sort array
 unsigned int i=0 ;
 unsigned int num_shifts = 16 ;  // 0xAA = 1 0 1 0   1 0 1 0   1 0 1 0    1 0 1 0    
 unsigned int count_of_bad_ones = 0;
 unsigned int bad_one_ui = 47825; // ok I know, this is bad, IRL, I'd do the typecasting

 while( i <= 10 ) {
   truncated_array[i] = data_to_sort[i] >> num_shifts ;
   cout << "the truncated array item is: " << truncated_array[i] << " ___ original item is: " << data_to_sort[i] << endl; 
       //count bad1s
       if(  truncated_array[i] == bad_one_ui  ){
	 count_of_bad_ones++; 
       }
       i++;
 }
     //
 cout << "==============================" << endl;
     cout << " total of " <<  count_of_bad_ones  << " found in data_to_sort" << endl;
     cout << "==============================" << endl;
     cout << " program complete " << endl;
     return 0;
  
}

 

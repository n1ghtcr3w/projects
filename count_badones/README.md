# cpp sort

## simple example to solve:

Please show in C how you would count the number of items in this array that match the following pattern: 0xBAD1_XXXX. The Xs represent don't cares.
unsigned int data_to_sort[] = {
0xBAD10002,
0xCAD20002,
0xBAC30002,
0x00000002,
0xBAD10001,
0xFFFF0002,
0xBAD10003,
0x00000002,
0x00000001,
0xBBD20005
};

#--code help from:
# https://www.programiz.com/python-programming/type-conversion-and-casting
# https://stackoverflow.com/questions/385572/typecasting-in-python
# https://www.w3schools.com/python/python_variables.asp
# https://www.quora.com/How-do-I-take-integer-input-in-Python-delimited-by-space?share=1
# https://pynative.com/python-check-user-input-is-number-or-string/
#-- converter app
# - more info on readme
# 

import os


def decode_input( dec_str ):
    print( "...decoding" )
    print( dec_str[0] )
    print( dec_str[1] )
    print( dec_str[2] )
    print( dec_str[3] )
    print( "\n" )

    #---------first value
    try:
        val = int( dec_str[0] )
        print( "1st input is an integer number. Number = ", val )
        from_int = dec_str[0]
    except ValueError:
        try:
            val = float( dec_str[0] )
            print( "1st input is a float number. Number = ", val )
            from_float = dec_str[0]
        except ValueError:
            print( "No.. 1st input is not a number. It's a string" )

    #------------ second value

    if( type( dec_str[1] ) == str ):
        print( "2nd input is a string" )
         
    # try:
    #     val = int( dec_str[1] )
    #     print( "No.. 2nd input is an integer number. please enter magnitude or type, valid types are (h,m,mi,k,km,au,pc) ", val )
    # except ValueError:
    #     try:
    #         val = float( dec_str[1] )
    #         print( "No.. 2nd input is a float number. please enter magnitude or type, valid types are (h,m,mi,k,km,au,pc) ", val )
    #     except ValueError:
    #         print( "Yes.. 2nd input is not a number. It's a string" )
    #         from_mag_or_unit = dec_str[1]

    #------------- second value
    # try:
    #     val = str( dec_str[1] )
    #     try:
    #         if( val / 1 == val):
    #             print( "Yes.. 2nd input is not a number. It's a string", val )
    #             from_mag_or_unit = dec_str[1]
    #     except ValueError:
            
    #     except ValueError:
    #         try:
    #             val = float( dec_str[1] )
    #             print( "No.. 2nd input is a float number. please enter magnitude or type, valid types are (h,m,mi,k,km,au,pc) ", val )
    #         except ValueError:
    #             try:
    #                 val = int( dec_str[1] )
    #                 print( "No.. 2nd input is an integer number. please enter magnitude or type, valid types are (h,m,mi,k,km,au,pc) ", val )
    #             except ValueError:
    #                 print( "No.. 2nd input is not a number or a string. It's a unknown. please enter magnitude or type, valid types are (h,m,mi,k,km,au,pc) ", val )

    #-------------------- test second value, part three

    # if( dec_str[1].isdigit() ):
    #     print( "2nd input is an integer or a float. please enter magnitude or type, valid types are (h,m,mi,k,km,au,pc) ", val )
    # else:
    #     from_mag_or_unit = dec_str[1]
    #     print( "2nd input is a magnitude or unit : ", val )
        
    return 



print( "---------- welcome to the km to mi distance converter -------------" )

while True:
    print( ":: distance format ( number unit_from to unit_to : " )
    in_str = raw_input( ":: for example ( 10 km to mi ) or ( 9 pc to mi : " ).split()

    # decode_input( in_str )      

    



    user_in = raw_input( "----do again ? (y) to continue----" )
    if( user_in.strip() != 'y'):
        break

    
print( "--------------- end of converter ----------" )
           

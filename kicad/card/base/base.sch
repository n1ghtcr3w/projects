EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Entry Bus Bus
	1100 1900 1200 2000
Entry Bus Bus
	1350 1600 1450 1700
Entry Bus Bus
	1100 2000 1200 2100
Entry Bus Bus
	1400 1650 1500 1750
Entry Bus Bus
	1100 1750 1200 1850
Entry Bus Bus
	1100 1650 1200 1750
Entry Bus Bus
	1100 1550 1200 1650
NoConn ~ 1450 1700
NoConn ~ 1400 1650
NoConn ~ 1500 1750
NoConn ~ 1150 1600
$EndSCHEMATC
